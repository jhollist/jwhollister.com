---
title: Easy Elevation for `rayshader` with `elevatr`
author: Jeffrey Hollister
draft: true
date: '2019-02-16'
categories:
  - R
  - GIS
tags:
  - spatial data
  - visualization
  - elevation
  - elevatr
  - rayshader
---

```{r}
library(rayshader)
library(elevatr)
library(raster)

data(lake)

lake_b <- rgeos::gBuffer(lake, width = 1000)

lake_dem <- get_elev_raster(lake_b, z=12, clip = "bbox")

lake_dem_m <- matrix(extract(lake_dem,raster::extent(lake_dem)),
               nrow=ncol(lake_dem),ncol=nrow(lake_dem))

lake_water <- detect_water(lake_dem_m)
lake_ray_shade <- ray_shade(lake_dem_m,zscale=50,lambert=FALSE)
lake_ambient_shade <- ambient_shade(lake_dem_m,zscale=50)

lake_dem_m %>%
  sphere_shade(texture = "desert") %>%
  add_water(lake_water, color="desert") %>%
  add_shadow(lake_ray_shade) %>%
  add_shadow(lake_ambient_shade) %>%
  plot_3d(lake_dem_m,zscale=10,fov=0,theta=135,zoom=0.75,phi=45, windowsize = c(1000,800))
render_snapshot()
```